"""
Functions for 2048 game.
"""
import random


UP = 1
DOWN = 2
LEFT = 3
RIGHT = 4

OFFSETS = {UP: (1, 0),
           DOWN: (-1, 0),
           LEFT: (0, 1),
           RIGHT: (0, -1)}


def left_shift(line):
    """Shift input line to the left"""
    new_line = [0 for dummy_num in range(len(line))]
    for line_index in range(len(line)):
        if line[line_index] != 0:
            for new_line_index in range(len(new_line)):
                if new_line[new_line_index] == 0:
                    new_line[new_line_index] = line[line_index]
                    break
    return new_line


def merge(line):
    """
    Function that merges a single row or column in 2048.
    """
    new_line = left_shift(line)
    out = [0 for dummy_num in range(len(new_line))]
    iteration = 0
    out[0] = new_line[0]
    while iteration < len(new_line):
        if iteration == 0 or new_line[iteration] == 0:
            iteration += 1
            continue
        if new_line[iteration - 1] == new_line[iteration]:
            out[iteration - 1] = new_line[iteration - 1] * 2
            new_line[iteration] = 0
        else:
            out[iteration] = new_line[iteration]
        iteration += 1
    return left_shift(out)


def traverse_grid(start_cell, direction, num_steps):
    """create a list of indexes for merge"""
    return [(start_cell[0] + step * direction[0], start_cell[1] + step * direction[1]) for step in range(num_steps)]


class TwentyFortyEight:
    """
    Class to run the game logic.
    """

    def __init__(self, grid_height, grid_width):
        self._grid_height = grid_height
        self._grid_width = grid_width
        self._board = []
        self.reset()
        self._start_cells = {UP: [(0, i) for i in range(self.get_grid_width())],
                             DOWN: [(-1, i) for i in range(self.get_grid_width())],
                             LEFT: [(i, 0) for i in range(self.get_grid_height())],
                             RIGHT: [(i, -1) for i in range(self.get_grid_height())]}

    def reset(self):
        """
        Reset the game so the grid is empty except for two
        initial tiles.
        """
        self._board = [[0 for dummy_col in range(self._grid_width)]
                       for dummy_row in range(self._grid_height)]
        self.new_tile()
        self.new_tile()

    def __str__(self):
        """
        Return a string representation of the grid for debugging.
        """
        output = ""
        for row in range(self.get_grid_height()):
            for col in range(self.get_grid_width()):
                output += str(self.get_tile(row, col)) + " "
            output += "\n"
        return output

    def get_grid_height(self):
        """
        Get the height of the board.
        """
        return self._grid_height

    def get_grid_width(self):
        """
        Get the width of the board.
        """
        return self._grid_width

    def move(self, direction):
        """
        Move all tiles in the given direction and add
        a new tile if any tiles moved.
        """
        number_of_steps = self.get_grid_height() if direction == 1 or direction == 2\
            else self.get_grid_width()

        required_indexes = [traverse_grid(self._start_cells[direction][start], OFFSETS[direction], number_of_steps)
                            for start in range(len(self._start_cells[direction]))]

        start_values = [[self.get_tile(sub_obj[0], sub_obj[1]) for sub_obj in obj] for obj in required_indexes]

        merged_values = [merge(start_values[step]) for step in range(len(start_values))]
        if start_values != merged_values:
            for row in range(len(required_indexes)):
                for coords in range(len(required_indexes[row])):
                    self.set_tile(required_indexes[row][coords][0], required_indexes[row][coords][1],
                                  merged_values[row][coords])
            self.new_tile()
        else:
            print("This move do nothing")

    def new_tile(self):
        """
        Create a new tile in a randomly selected empty
        square.  The tile should be 2 90% of the time and
        4 10% of the time.
        """
        zero_counter = 0
        for row in self._board:
            zero_counter += row.count(0)
        if zero_counter > 0:
            zero_indexes = [(row, col)
                            for row in range(self.get_grid_height())
                            for col in range(self.get_grid_width())
                            if self.get_tile(row, col) == 0]
            selected_cell = random.choice(zero_indexes)
            random_val = random.randint(1, 100)
            val_to_set = 2 if random_val <= 90 else 4
            self.set_tile(selected_cell[0], selected_cell[1], val_to_set)

    def set_tile(self, row, col, value):
        """
        Set the tile at position row, col to have the given value.
        """
        self._board[row][col] = value

    def get_tile(self, row, col):
        """
        Return the value of the tile at position row, col.
        """
        return self._board[row][col]
